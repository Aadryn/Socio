﻿// Guids.cs
// MUST match guids.h
using System;

namespace Socio.Socio_Presentation_Package
{
    static class GuidList
    {
        public const string guidSocio_Presentation_PackagePkgString = "3b68e0ba-625a-4c8d-8cc0-ba0bec8d721d";
        public const string guidSocio_Presentation_PackageCmdSetString = "8f1b6d7f-9782-4e63-8844-c874c3fd2e8e";
        public const string guidToolWindowPersistanceString = "b50b2a80-ae2f-438c-a464-155f0f2d43d6";
        public const string guidSocio_Presentation_PackageEditorFactoryString = "c6468f44-3368-4561-88e5-fe227f6cc142";

        public static readonly Guid guidSocio_Presentation_PackageCmdSet = new Guid(guidSocio_Presentation_PackageCmdSetString);
        public static readonly Guid guidSocio_Presentation_PackageEditorFactory = new Guid(guidSocio_Presentation_PackageEditorFactoryString);
    };
}